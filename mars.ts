
let alasql = null;
import { connectTo, connectFrom }  from './sockets';
import { MsgHandlerTo, MsgHandlerFrom, MsgType,
    frzInsertValuesRequest, frzDeleteRecordRequest, frzCallServerMethodRequest,
    RequestState,
    OptUpdateReq, OptUpdateRep, encodeOptUpdateReq,
    UpdateRecord,
    SubscribeReq, SubscribeRep, encodeSubscribeReq,
} from './msg';

// keep it in sync with the function in server.d
interface IIndexStatementFor {
    (table: number, op : string): number;
};

var indexStatementFor: IIndexStatementFor; 


export class MarsClient 
{
    url: string;
    identifier: number;
    db: any;
    msgId: number;
    messages: any;
    $store: any;
    clientid: string;
    state: string;
    socketTo: WebSocket; // socket to use for request to the service
    socketFrom: WebSocket; // socket with request coming from the service
    msgHandlerTo: MsgHandlerTo;
    msgHandlerFrom: MsgHandlerFrom;
    sqlStatements: Array<string>;
    onConnected: any;
    jsStatements: Array<any>;
    schema: Array<Object>;
    isMetaSyncTable: Array<boolean>; // is the table decorated with metadata about sync?
    isCachedTable: Array<boolean>; // is the table cached in the server and in the client?

    /**
     * The client is instantiated only one time, when the mars support is installed */
    constructor(url) {
        this.url = url;
        // ... the identifier is used by the server to understand if a client is reconnecting after that the
        //     connection was lost ... that behaviour is usual in development, when the server is restarted
        //     often during the edit-compile-run cycle ...
        this.identifier = Math.floor((Math.random() * 1000) + 1);

        this.db = new alasql.Database('mars');
        
        this.msgId = 0; // used to track message request/response
        this.messages = new Map();
        this.onConnected = () => console.log('onConnected');
    }

    initializeCommunication(store, onConnected) {
        this.onConnected = onConnected;
        this.$store = store;
        connectTo(this.url, this);
    }

    // a socket connection is now ready, handshake with the server 
    beginComunication() {
        console.log('mars - starting to handshake with the server');
        
        this.clientid = 'client_' + this.identifier.toString();

        this.state = 'helo';
        this.socketTo.send('mars');
    }

    onMessageTo(msg): void {
        //console.log('mars - received message from websocket:"%s"', msg.data);
        if( this.state === 'helo' ){
            if( msg.data === 'marsserver0000' ){
                console.log('C <-- S | helo');
                this.state = 'identifying';
                console.log('C --> S | client id %s', this.clientid);
                this.socketTo.send(this.clientid);
            }
            else {
                console.log('C <-- S | unexpected reply from mars server (data:%s), disconnecting', msg.data);
                this.socketTo.close();
                this.socketFrom.close();
                this.$store.commit('setConnectionStatus', 'waitingForReconnection');
            }
        }
        else if( this.state === 'identifying' ){
            if( msg.data === 'marswelcome' || msg.data === 'marsreconnected' ){
                console.log('C <-- S | marswelcome / marsreconnected');
                this.state = 'connected';
                // ... we are ready, from now on, the communication is handled by the message handler
                this.msgHandlerTo = new MsgHandlerTo(this.socketTo, this);
                connectFrom(this.url, this);
                this.msgHandlerFrom = new MsgHandlerFrom(this.socketFrom, this);
                // ... let's signal the app that we are connected to the server ...
                this.$store.commit('setConnectionStatus', 'waitingForMessages');
                // XXX ???? ma da dove viene? this.onConnected();
                const credential = this.$store.state.mars.credential;
                if( credential.authorised === true ){
                    console.log('trying to re-authorise the user %s', credential.username);
                    this.$store.dispatch('loginUser', credential.username, credential.password);
                }
            }

            else {
                console.log('mars - unexpected reply from mars server, disconnecting');
                this.socketTo.close();
                this.socketFrom.close();
                this.$store.commit('setConnectionStatus', 'waitingForReconnection');
            }
        }
    }

    createDatabase(sql) {
        //console.log('mars - creating database:%s', sql);
        this.db.exec(sql);
        //console.log('mars - database ready to be filled');
    }

    dropDatabase() {
        //console.log('mars - creating database:%s', sql);
        const tables = this.db.exec("SHOW TABLES");
        tables.forEach(table => this.db.exec("DROP TABLE " + table.tableid));
        //console.log('mars - database ready to be filled');
    }
    
    prepareStatements(sqlStatements) {
        console.log('mars - preparing sql statements');
        this.sqlStatements = new Array();
        for( let i =0; i <sqlStatements.length; ++i ){
            console.log('mars - statement:%d %s', i, sqlStatements[i]);
            let stat = alasql.compile(sqlStatements[i], 'mars');
            this.sqlStatements.push(stat);
        }
        console.log('mars - prepared %d sql statements', this.sqlStatements.length);
    }

    prepareJsStatements(jsStatements) {
        console.log('mars - preparing javascript statements');
        indexStatementFor    = eval(jsStatements[0]);
        this.isMetaSyncTable = eval(jsStatements[1]);
        this.isCachedTable   = eval(jsStatements[2]);
        this.schema          = JSON.parse(jsStatements[3]);
        this.jsStatements = new Array();
        for( let i =4; i < jsStatements.length; ++i ){
            console.log('mars - statement:%s', jsStatements[i]);
            let stat: any = eval(jsStatements[i]);
            this.jsStatements.push(stat);
        }
    }

    asPkParamStruct(table: number, key: Object): any      { return this.jsStatements[table *3 +0](key); }
    asPkParamWhereStruct(table: number, key: Object): any { return this.jsStatements[table *3 +1](key); }
    referencesFor(table: number): any                     { return this.jsStatements[table *3 +2](); }

    importRecords(tableIndex: number, statementIndex: number, records: Array<Object>): void {
        this.$store.commit('setDataStatus', 'loading');
        const sql: any = this.sqlStatements[statementIndex];
        for(let i =0; i <records.length; ++i ){
            const record = records[i];
            //console.log('and values are:', record);
            try {
                sql(record);
            } 
            catch(e) {
                // ... this is now needed as we have not-lazy table that are referring to not loaded 'lazy' table:
                //     foreign key errors.
            }
        }
        this.triggerUpdates(tableIndex);
        //console.log('mars - imported records into table');
    }

    deleteRecords(tableIndex: number, statementIndex: number, records: Array<Object>): void {
        console.log('mars - deleting values from table');
        const sql: any = this.sqlStatements[indexStatementFor(tableIndex, 'updateDecorations')];
        for(let i=0; i <records.length; ++i ){
            const record = records[i];
            //console.log("record:", record);
            sql(record);
        }
        this.triggerUpdates(tableIndex);
        setTimeout( () => {
            const sql: any = this.sqlStatements[indexStatementFor(tableIndex, 'delete')];
            for(let i = 0; i < records.length; ++i ){
                const record = records[i];
                sql(record);
            }
            this.triggerUpdates(tableIndex);
        }, 2000);
    }

    insertRecords(tableIndex: number, statementIndex: number, records: Array<Object>): void {
        if( ! this.isCachedTable[tableIndex] ) alert('unexpected insert in not cached table.');
        console.log('mars - inserting values into table');
        const sql: any = this.sqlStatements[statementIndex];
        for(let i =0; i <records.length; ++i ){
            const record = records[i];
            try {
                sql(record);
            }
            // XXX this is a problem! right now, we are receiving from server an insert also
            // for a client side insert performed by this client. must be handled server side.
            catch(e){
                console.log('tacheeeee!', e);
            }
        }
        //console.log('mars - inserted values into table');
        this.triggerUpdates(tableIndex);
    }

    updateRecords(tableIndex: number, records: Array<UpdateRecord>): void {
        if( ! this.isCachedTable[tableIndex] ) alert('unexpected update record from server for a not cached table.');
        const sql: any = this.sqlStatements[indexStatementFor(tableIndex, 'updateDecoratedRecord')];
        for(let i =0; i < records.length; ++i ){
            const record = records[i];
            const keys = this.asPkParamWhereStruct(tableIndex, record.keys);
            sql(Object.assign({}, record.record, keys));

        }
        this.triggerUpdates(tableIndex);
    }
    
    // server side update of the value, here we have the notification
    updateValues(tableIndex: number, statementIndex: number,values) {
        //console.log('mars - updating values into table');
        const sql: any = this.sqlStatements[statementIndex];
        for(let i =0; i <values.length; ++i ){
            const vvv = values[i];
            //console.log('and values are:', vvv);
            let args = vvv.record;
            for(var key in vvv.keys) args["key" + key]=vvv.keys[key];
            //console.log('and arguments are:', args);
            sql(args);
        }
        this.triggerUpdates(tableIndex);
        //console.log('mars - updated values into table');
    }


    vueInsertRecord(table: number, record: any){
        const tableIndex = table;

        const clonedRecord = JSON.parse(JSON.stringify(record));

        // ... let's insert optimistically the record in alasql if the table is cached
        if( this.isCachedTable[tableIndex] ){
            record.mars_who = this.$store.state.mars.credential.username + '@' + this.clientid;
            record.mars_when = new Date().toString()
            record.mars_what = 'opt_insert';


            const sql: any = this.sqlStatements[indexStatementFor(tableIndex, "insert")];
            try {
                sql(record);
            }
            catch(e){
                if(e.message === 'Cannot insert record, because it already exists in primary key index'){
                    alert('Cannot insert record, because it already exists in primary key index');
                    return;
                }
            }
        }
    
        // ... let's send the request to the server
        this.msgHandlerTo.sendMessageRequest(MsgType.insertValuesRequest, frzInsertValuesRequest(tableIndex, clonedRecord));
        // ... if we are not cached, we need to wait the reply
        if( this.isCachedTable[tableIndex] ) this.triggerUpdates(tableIndex);
    }

    onVueInsertRecordReply(msgRequest, msgReply)
    {
        if( msgReply.statementIndex == -1 ){
            console.log("The server failed to unpack the record to insert!");
            alert("The server failed to unpack the record to insert.");
        }
        else {
            console.log("the reply I've received", msgReply);
            if( msgReply.insertStatus === 1 ){   // inserted
                if( this.isCachedTable[msgReply.tableIndex] ){
                    const sql: any = this.sqlStatements[msgReply.statementIndex];
                    const parameters = Object.assign({},  msgReply.bytes, msgReply.clientKeys);
                    ///** Deep clone obects */ // in alasql: Uint8Array, dobbiamo supportarlo....
                    sql(parameters);
                    // right now, update as a second step the decorations, can be merged with the above if we provide a sql
                    const sql2: any = this.sqlStatements[msgReply.statementIndex2];
                    sql2(Object.assign({}, { mars_who: this.$store.state.mars.credential.username + '@' + this.clientid, mars_when: new Date().toString(), mars_what: 'inserted' }, msgReply.clientKeys));
                }
                this.triggerUpdates(msgReply.tableIndex);
            }
            else if( msgReply.insertStatus === 2 ){ // duplicateKeyViolations
                alert('violazione chiave duplicata, lato server');
                if( this.isCachedTable[msgReply.tableIndex] ){
                    // update the decorations, signal the duplicate key
                    const sql: any = this.sqlStatements[msgReply.statementIndex];
                    const parameters = Object.assign({}, { mars_who: this.$store.state.mars.credential.username + '@' + this.clientid, mars_when: new Date().toString(), mars_what:'duplicateKeyViolations'}, msgReply.clientKeys);
                    console.log('parameters:', parameters);
                    sql(parameters);
                    this.triggerUpdates(msgReply.tableIndex);
                    // delete the record after a while
                    setTimeout( () => {
                        console.log('deleting...', msgReply);
                        const sql: any = this.sqlStatements[msgReply.statementIndex2];
                        sql(msgReply.clientKeys);
                        this.triggerUpdates(msgReply.tableIndex);
                        console.log('deleted', this);
                    }, 2000);
                }
            }
            else if( msgReply.insertStatus === 3 ){ // unknown server errors
                alert('server side unknown error on insert'); // XXX curry record with proper information
                if( this.isCachedTable[msgReply.tableIndex] ){
                    const sql: any = this.sqlStatements[msgReply.statementIndex];
                    sql(msgReply.clientKeys);
                }
                this.triggerUpdates(msgReply.tableIndex);
            }
        }
    }

    vueDeleteRecord(tableIndex: number, record: Object): void {
        if( this.isCachedTable[tableIndex] ){
            const sql: any = this.sqlStatements[indexStatementFor(tableIndex, "delete")];
            let k = this.asPkParamWhereStruct(tableIndex, record);
            console.log('mars - deleting using sql statement ', indexStatementFor(tableIndex, "delete"), " with where ", k);
            sql(k); // XXX quando può fallire? vedi sopra, in vueInsertRecord
        }

        // ... let's send the request to the server
        const keys = this.asPkParamWhereStruct(tableIndex, record);
        this.msgHandlerTo.sendMessageRequest(MsgType.deleteRecordRequest, frzDeleteRecordRequest(tableIndex, keys));
        this.triggerUpdates(tableIndex);
    }

    onVueDeleteRecordReply(msgRequest, msgReply){
        //console.log(msgRequest);
        if( msgReply.statementIndex == -1 ){
            console.log("The server failed to unpack the record to delete!");
            alert("The server failed to unpack the record to delete");
        }
        else {
            //console.log(msgReply);
            if( msgReply.deleteStatus == 1 ){ // deleted
                if( ! this.isCachedTable[msgRequest.tableIndex] ){
                    this.triggerUpdates(msgRequest.tableIndex);
                }
            }
            else if( msgReply.deleteStatus == 2 ){ // unknown server error
                alert('server side unknown errro on delete'); // XXX curry record with info
                if( this.isCachedTable[msgRequest.tableIndex] ){
                    const sql: any = this.sqlStatements[msgReply.statementIndex];
                    sql(msgReply.serverRecord);
                }
                this.triggerUpdates(msgRequest.tableIndex);
            }
        }
    }

    vueUpdateRecord(tableIndex: number, keys: Object, record: Object): void {
        if( JSON.stringify(this.asPkParamStruct(tableIndex, keys)) !== JSON.stringify(keys)){
            alert("wrong keys in update! (wrong names, or some key is missing)");
            console.error(JSON.stringify(this.asPkParamStruct(tableIndex, keys)));
            console.error(JSON.stringify(keys));
            return;
        }
        // ... switch from { a: 1 } to { keya: 1 }
        const whereKeys = this.asPkParamWhereStruct(tableIndex, keys);
        // ... as we eventually need to access the Sync metadata when processing the reply
        let sync = {};

        // ... optimistically update the table, but saving the original object, if it's cached
        let sql: any = this.sqlStatements[indexStatementFor(tableIndex, "selectFromWhere")];
        let originalRecord: Object = sql(whereKeys);
        if( this.isCachedTable[tableIndex] ){
            if(this.isMetaSyncTable[tableIndex]){
                sql = this.sqlStatements[indexStatementFor(tableIndex, "update")];
                sql(Object.assign({}, record, whereKeys));
                sql = this.sqlStatements[indexStatementFor(tableIndex, "updateDecorations")];
                sync = {
                    mars_who: this.$store.state.mars.credential.username + '@' + this.clientid,
                    mars_when: new Date().toString(),
                    mars_what:'optupdated'
                };
                sql(Object.assign({}, sync, whereKeys));
            }
            else {
                const sql: any = this.sqlStatements[indexStatementFor(tableIndex, "update")];
                sql(Object.assign({}, record, whereKeys));
            }
        }

        // ... let's send the request to the server
        const req: OptUpdateReq = { tableIndex, keys, record, sync, originalRecord };
        this.msgHandlerTo.sendReq(req, encodeOptUpdateReq(req));
        this.triggerUpdates(tableIndex);
    }

    onVueUpdateReply(req: OptUpdateReq, rep: OptUpdateRep): void 
    {
        if( rep.state === RequestState.rejectedAsWrongParameter ||
            rep.state === RequestState.rejectedAsDecodingFailed 
        ){
            const msg: string = "bug: optimistic update failed:" + rep.state; 
            console.error(msg);
            // ... if the table is cached, patch it locally
            if( this.isCachedTable[req.tableIndex] ){
                if(this.isMetaSyncTable[req.tableIndex]){
                    const updatedKeys = this.asPkParamWhereStruct(req.tableIndex, req.record);
                    let sql: any = this.sqlStatements[indexStatementFor(req.tableIndex, "update")];
                    sql(Object.assign({}, req.originalRecord, updatedKeys));
                }
                else {
                    // extract the keys of the optimistically updated record
                    const whereKeys: Object = this.asPkParamWhereStruct(req.tableIndex, req.record);
                    const sql: any = this.sqlStatements[indexStatementFor(req.tableIndex, "update")];
                    sql(Object.assign({}, req.originalRecord, whereKeys));
                }
            }
        }
        // ... success, the server has updated the table ...
        else if( rep.state === RequestState.executed ){
            if( this.isCachedTable ){
                // extrack the keys of the optimistically updated record
                const whereKeys: Object = this.asPkParamWhereStruct(req.tableIndex, req.record);
                // XXX should I update also all the other fields? yep! Where is the server updated record? Check!
                if(this.isMetaSyncTable[req.tableIndex]){
                    const sql: any = this.sqlStatements[indexStatementFor(req.tableIndex, "updateDecorations")];
                    sql(Object.assign({}, { mars_what: "updated", mars_when: req.sync.mars_when, mars_who: req.sync.mars_who }, whereKeys));
                }
                this.triggerUpdates(req.tableIndex);
            }
            // ... if the table is not cached, re-execute the query.
            //     server-side, we are not able to send just the diff right now, and in any case, a re-execution of the
            //     query is unavoidable
            else {
                this.triggerUpdates(req.tableIndex);
            }
        }
        else {
            const msg: string = "bug: unexpected state";
            console.error(msg); console.error(msg);
        }
    }

    // if the table was subscribed, trigger an update
    triggerUpdates(tableIndex: number){
        if( this.subscriptions.has(tableIndex) ){
            // ... some VM is subscribed to this table
            for( let [vm, toUpdates] of this.subscriptions.get(tableIndex) ){
                // ... this vm is subscribed, more that one query in the VM to update
                for( let toUpdate of toUpdates ){
                    toUpdate();
                }
            }
        }
    }

    subscribe(vmName: number, table: number, updateDatas: Array<UpdateData> ): void 
    {
        // extract the subscriptions for that table
        let a : Map<number, Array<UpdateData> >;
        if( this.subscriptions.has(table) ){
            a = this.subscriptions.get(table);
        }
        else {
            a = new Map<number, Array<UpdateData> >();
            this.subscriptions.set(table, a);
        }

        // sanity check
        if( a.has(vmName) ) alert("this vm is already subscribed to this table?, replacing...");

        // set the new subscription
        a.set(vmName, updateDatas);
    }

    unsubscribe(vmName: number, table: number): void
    {
        if( ! this.subscriptions.has(table) ){ alert("unsubscribe of not subscribed table?"); return; }
        let sub = this.subscriptions.get(table);
        if( ! sub.has(vmName) ){ alert("unsubscribe of not subscribed vm?"); return; }
        sub.delete(vmName);
    }

    subscriptions: Map<number, Map<number, Array<UpdateData> > > = new Map<number, Map<number, Array<UpdateData> > >();
}
interface UpdateData {
        (): void;
}
var theMarsClient = null;

class DollarMars 
{
    vm: any;
    
    dataName: string;
    select: string; 
    table: number;
    parameters: Function;

    constructor(vm) { this.vm = vm; }
    client() { return theMarsClient; }

    // the web client is requesting an insert.
    // Params:
    //     table = the table numeric identifier
    //     record = Json object with _all_ the columns and _all_ the values to insert
    insertRecord(table: number, record: Object){
        theMarsClient.vueInsertRecord(table, record);
    }


    // the web client is requesting a delete.
    // Params:
    //     table = the table numeric identifier
    //     record = Json object with _all_ the columns and _all_ the values to delete.
    deleteRecord(table: number, record: Object){
        theMarsClient.vueDeleteRecord(table, record);
    }

    // the web client is requesting an update of a record.
    // Params:
    //     table = the table numeric identifier
    //     keys = Json object with the primary keys values of the record to update.
    //     record = Json object with _all_ the new values of the record.
    updateRecord(table: number, keys: Object, record: Object){
        theMarsClient.vueUpdateRecord(table, keys, record);
    }

    /**
     * Handle the mars options defined in the vue component, during the creation ...
     * 
     * @param name - the name of the data in the vm (ex. people)
     * @param options - json with the options for that data (ex, { select: 'select * ..', table: 2, .. })
     */
    option(name, options) {
        //console.log('mars.DollarMars.option(name:%s, options:%s)', name, options);

        // safety checks
        if( ! ("table" in options) ){ alert('missing table number in mars declaration! '+ JSON.stringify(options)); }
        for(let property in options.parameters) {
            if(options.parameters[property] === null) {
                alert('Parameter value is "null" '+ JSON.stringify(options));
            }
        }

        this.dataName = name;
        this.select = options.select;
        this.table = options.table;
        this.parameters = options.parameters;

        // ... when there's the need to refresh the data, this delegate is called
        const updateTheData = () => {
            if( options.table >= theMarsClient.isCachedTable.length ) alert('wrong table number in query: '+ options.table);

            let records = [];
            if(! theMarsClient.isCachedTable[options.table]){ // XXX and for complex query?
                if( ! options.parameters ){
                    const req: SubscribeReq = { select: options.select, parameters: "", vm: this.vm, name, columns: options.columns };
                    theMarsClient.msgHandlerTo.sendReq(req, encodeSubscribeReq(req));
                }
                else {
                    const req: SubscribeReq = { select: options.select, parameters: JSON.stringify(options.parameters.bind(this.vm)()), vm: this.vm, name, columns: options.columns };
                    theMarsClient.msgHandlerTo.sendReq(req, encodeSubscribeReq(req));
                }
                this.vm[name + '_mars_status'] = { loading: true };
            }
            else {
                if( ! options.parameters ){ // ,, use 'options' and not 'this', this is a lambda
                    records = theMarsClient.db.exec(options.select);
                }
                else {
                    let compiled = alasql.compile(options.select, 'mars');
                    records = compiled(options.parameters.bind(this.vm)());
                }
            }
            this.vm[name] = records;
            this.vm[name + "_mars_definition"] = { references: theMarsClient.referencesFor(options.table) };
        }

        // ... refresh the data if the parameters of the query were modified
        if( options.parameters ){
            this.vm.$watch(options.parameters, updateTheData, { deep: true });
        }
    
        // ... register the updater delegate, so that Mars can update the data when the server push some operation
        let a : Array<UpdateData>;
        if( this.subscriptions.has(options.table) ){
            a = this.subscriptions.get(options.table);
        }
        else {
            a = new Array<UpdateData>();
            this.subscriptions.set(options.table, a);
        }
        a.push( () => updateTheData() );
    }

    // once we have collected all the options declared in the VM, now subscribe via the marsClientInstance
    registerSubscriptions(){
        if( this.subscriptions.size == 0 ) console.log('mars - warn, mars component but no subscription to data');
        let toBeUpdated: UpdateData[] = [];
        for( let [table, updates] of this.subscriptions ){
            this.client().subscribe(this.vm._uid, table, updates);
            toBeUpdated = toBeUpdated.concat(updates);
        }

        // ... check if we can update right now ...
        if( theMarsClient.$store.state.mars.dataStatus == 'waitingForOperations' ){
            for( let update of toBeUpdated ){ update(); }
        }
        // ... if not, check again after one second, for ten times
        else {
            let howManyTimes = 0;
            const handle = setInterval( () => {
                console.log('%d, check if the db is idle... ', howManyTimes, theMarsClient.$store.state.mars.dataStatus);
                if( theMarsClient.$store.state.mars.dataStatus == 'waitingForOperations' ){
                    for( let update of toBeUpdated ){ update(); }
                    clearInterval(handle);
                }
                else {
                    howManyTimes ++;
                    if( howManyTimes >= 30 ){
                        clearInterval(handle);
                        console.log('the database is not ready for operations, I give up!');
                    }
                }
            }, 1000);
        }
    }

    subscriptions: Map<number, Array<UpdateData> > = new Map<number, Array<UpdateData> >();

}

const mixinDollarMars = function()
{
    //console.log('mars.mixinDollarMars(this:%s)', this);
    Object.defineProperty(this, '$mars', {
        get: () => {
            if( ! this._mars ){ this._mars = new DollarMars(this); }
            return this._mars;
        }
    });
};

const scanComponentForMars = function() {
    //console.log('mars.scanComponentForMars(this:%s)', this);

    // this is the 'mars:' attribute in the Vue components...
    let mars = this.$options.mars;

    if( mars ){
        //console.log('mars.scanComponentForMars(this:%s) - the component is using Mars.', this);
        for( let key in mars ){
            this.$mars.option(key, mars[key]); // process the mars definition in the component
        }
        this.$mars.registerSubscriptions();
    }
};

const removeSubscriptions = function() {
   for( let table of this.$mars.subscriptions.keys() ){
      theMarsClient.unsubscribe(this._uid, table);
   } 
}

interface Credential {
    username: String;
    password: String;
}

interface ServerMethod {
    method: string;
    parameters: Object;
    callback: Function;
}

export 
let marsStore = 
{
    state: {
        connectionStatus: 'connecting', // waitingForReconnection, waitingForMessages, handlingMessages
        dataStatus: 'waitingForLoading', // 'loading', 'waitingForOperations'
        credential: { username: '', password: '', authorised: false, errors: '' },
        serverMethod: { method: '', parameters: '', returns: '', callback: null },
    },
    getters: {
        authorised: function(state) { return state.credential.authorised; },
    },
    mutations: {
        setConnectionStatus(localState, connectionStatus) {
            localState.connectionStatus = connectionStatus;
            //console.log('mars - store connection status set to %s', connectionStatus);
        },
        setDataStatus(localState, dataStatus) {
            localState.dataStatus = dataStatus;
        },

        authenticateUser(localState, credential: Credential){
            if( credential.username != localState.credential.username || credential.password != localState.credential.password ){
                localState.credential = { username: credential.username, password: credential.password, authorised: false };
            }
            else console.log('mars - same credential for current user, that is authenticated:%s', localState.credential.authorised);
            localState.credential.errors = '';
        },

        authorizeUser(localState){
            localState.credential = { 
                username: localState.credential.username, 
                password: localState.credential.password, 
                authorised: true, errors: '' 
            };
        },

        deauthorizeUser(localState, { errors }: { errors: string } ){
            localState.credential = { 
                username: localState.credential.username, 
                password: localState.credential.password, 
                authorised: false, errors: errors 
            };
        },

        setCurrentServerMethod(localState, serverMethod: ServerMethod) {
            localState.serverMethod = serverMethod;
        },

        callServerMethodReply(localState, args: any[]){
            localState.serverMethod.method = args[0];
            localState.serverMethod.parameters = args[1];
            localState.serverMethod.returns = args[2];
            if(localState.serverMethod.callback)
                localState.serverMethod.callback();
        }
    },
    actions: {
        loginUser(context, credential: Credential){
            context.commit('authenticateUser', credential);
            console.log('C --> S | authenticate username:%s', credential.username);
            theMarsClient.msgHandlerTo.sendMessageRequest(0, { username: credential.username });
        },
        logoutUser(context){
            context.commit('deauthorizeUser', { errors: "" });
            theMarsClient.msgHandlerTo.sendMessageRequest(4, {}); 
        },
        callServerMethod(context, serverMethod: ServerMethod ){
            console.log('C --> S | call server method:%s parameters:%s', serverMethod.method, serverMethod.parameters, serverMethod.callback );
            context.commit('setCurrentServerMethod', serverMethod);
            theMarsClient.msgHandlerTo.sendMessageRequest(150, frzCallServerMethodRequest(serverMethod.method, serverMethod.parameters));
        }
    }
};

export function marsInstall(Vue: any, options: any): void
{
    // ... I wasn't able to require directly alasql in this module, so I'm passing it from the main js
    alasql = options.alasql;

    // ... this will last all the session, and it's not re-instantiated if the connection with the mars server is
    //     dropped or lost ...
    theMarsClient = new MarsClient(options.url);

    Vue.mixin({
        beforeCreate: mixinDollarMars, // ... inject mars into every vue components ...
        created: scanComponentForMars, // ... verify if the component is actually using mars ...
        beforeDestroy: removeSubscriptions, // ... remove subscriptions for refresh of sql data
    });
};

